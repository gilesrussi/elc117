vitima(anita).
pobre(bia).
pobre(pedro).
pobre(bernardo).
pobre(maria).
rico(caren).
rico(alice).
rico(henrique).
rico(adriano).
relacionamento(anita, bernardo).
relacionamento(bernardo, caren).
relacionamento(anita, pedro).
relacionamento(pedro, alice).
relacionamento(henrique, alice).
relacionamento(henrique, maria).
relacionamento(maria, adriano).
relacionamento(adriano, caren).

localizacao(pedro, santamaria, segunda).
localizacao(pedro, santamaria, terca).
localizacao(pedro, portoalegre, quarta).
localizacao(pedro, santamaria, quinta).
localizacao(pedro, apartamento, sexta).
localizacao(caren, portoalegre, segunda).
localizacao(caren, portoalegre, terca).
localizacao(caren, portoalegre, quarta).
localizacao(caren, santamaria, quinta).
localizacao(caren, apartamento, sexta).
localizacao(henrique, apartamento, segunda).
localizacao(henrique, portoalegre, terca).
localizacao(henrique, apartamento, quarta).
localizacao(henrique, apartamento, quinta).
localizacao(henrique, apartamento, sexta).
localizacao(bia, apartamento, segunda).
localizacao(bia, portoalegre, terca).
localizacao(bia, portoalegre, quarta).
localizacao(bia, santamaria, quinta).
localizacao(bia, apartamento, sexta).
localizacao(adriano, apartamento, segunda).
localizacao(adriano, apartamento, terca).
localizacao(adriano, santamaria, quarta).
localizacao(adriano, apartamento, quinta).
localizacao(adriano, apartamento, sexta).
localizacao(alice, apartamento, segunda).
localizacao(alice, portoalegre, terca).
localizacao(alice, portoalegre, quarta).
localizacao(alice, apartamento, quinta).
localizacao(alice, apartamento, sexta).
localizacao(bernardo, santamaria, segunda).
localizacao(bernardo, santamaria, terca).
localizacao(bernardo, portoalegre, quarta).
localizacao(bernardo, santamaria, quinta).
localizacao(bernardo, apartamento, sexta).
localizacao(maria, apartamento, segunda).
localizacao(maria, santamaria, terca).
localizacao(maria, santamaria, quarta).
localizacao(maria, santamaria, quinta).
localizacao(maria, apartamento, sexta).
insanidade(maria).
insanidade(adriano).

casal(X, Y) :- relacionamento(X, Y).
casal(X, Y) :- relacionamento(Y, X).
ciumes(X, Y) :- casal(Z, X), casal(Y, Z).
ciumenta(X) :- ciumes(X, Y), vitima(Y).
motivo(X) :- pobre(X); insanidade(X); ciumenta(X).

acesso_bastao(X) :- localizacao(X, santamaria, quarta); localizacao(X, portoalegre, quinta).
acesso_martelo(X) :- localizacao(X, apartamento, quarta); localizacao(X, apartamento, quinta).
acesso_arma(X) :- acesso_martelo(X); acesso_bastao(X).
acesso_chave(X) :- localizacao(X, santamaria, segunda); localizacao(X, portoalegre, terca).
acesso_apartamento(X) :- localizacao(X, apartamento, quinta); localizacao(X, apartamento, sexta).
acesso(X) :- acesso_arma(X), acesso_chave(X), acesso_apartamento(X).

assassino(X) :- motivo(X), acesso(X).
