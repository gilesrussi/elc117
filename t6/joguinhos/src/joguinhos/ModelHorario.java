/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package joguinhos;



/**
 *
 * @author SAA
 */
public class ModelHorario {
    public static int INDISPONIVEL = 0;
    public static int LIVRE = 1;
    public static int OCUPADO = 2;
    
    private Integer[][] horarios;
    
    public ModelHorario() {
        horarios = new Integer[7][24];
        for(int i = 0; i < 7; i++) {
            for(int j = 0; j < 24; j++) {
                horarios[i][j] = 0;
            }
        }
    }
    
    public Boolean isLivre(ModelHorario h) {
        int i, j;
        for(i = 0; i < 7; i++) {
            for(j = 0; j < 24; j++) {
                if(h.getHorarios()[i][j] == LIVRE && this.horarios[i][j] != LIVRE) {
                    return false;
                }
                
            }
        }
        return true;
    }
    
    public void ocupar(ModelHorario h) {
        int i, j;
        for(i = 0; i < 7; i++) {
            for(j = 0; j < 24; j++) {
                if(h.getHorarios()[i][j] != INDISPONIVEL) {
                    this.horarios[i][j] = OCUPADO;
                }
            }
        }
    }
    
    public void ocupar(Integer dia, Integer hora) {
        this.horarios[dia][hora] = OCUPADO;
    }
    
    public void liberar(ModelHorario h) {
        int i, j;
        for(i = 0; i < 7; i++) {
            for(j = 0; j < 24; j++) {
                if(h.getHorarios()[i][j] != INDISPONIVEL) {
                    this.horarios[i][j] = LIVRE;
                }
            }
        }
    }
    
    public void liberar(Integer dia, Integer hora) {
        this.horarios[dia][hora] = LIVRE;
    }
    
    public Integer[][] getHorarios() {
        return this.horarios;
    }
    
    @Override
    public String toString() {
        String s = "";
        for(int i = 0; i < 7; i++) {
            s += Integer.toString(i) + " - ";
            for(int j = 0; j < 24; j++) {
                s += (Integer.toString(this.horarios[i][j]) + " ");
            }
            s += "\n";
        }
        return s;
    }
}
