/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package joguinhos;

import java.util.ArrayList;

/**
 *
 * @author SAA
 */
public class ModelPersonagem {
    private String nome_jogador;
    private String nome_personagem;
    private Integer level;
    private String raca;
    private String classe;
    private String profissao;
    private Integer profissao_level;
    private String especialidade;
    private ModelHorario horarios;
    private ArrayList<ModelGrupo> grupos;

    public void setNome_jogador(String nome_jogador) {
        this.nome_jogador = nome_jogador;
    }

    public void setNome_personagem(String nome_personagem) {
        this.nome_personagem = nome_personagem;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public void setProfissao_level(Integer profissao_level) {
        this.profissao_level = profissao_level;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public String getNome_jogador() {
        return nome_jogador;
    }

    public String getNome_personagem() {
        return nome_personagem;
    }

    public Integer getLevel() {
        return level;
    }

    public String getRaca() {
        return raca;
    }

    public String getClasse() {
        return classe;
    }

    public String getProfissao() {
        return profissao;
    }

    public Integer getProfissao_level() {
        return profissao_level;
    }

    public String getEspecialidade() {
        return especialidade;
    }
    
    public ModelPersonagem() {
        this.horarios = new ModelHorario();
        this.grupos = new ArrayList<ModelGrupo>();
    }
    
    public void setHorarios(ModelHorario horario) {
        this.horarios = horario;
    }
    
    public ModelHorario getHorarios() {
        return this.horarios;
    }
    
    public void addGrupo(ModelGrupo g) {
        this.grupos.add(g);
    }
    
    public void removeGrupo(ModelGrupo g) {
        this.horarios.liberar(g.getHorarios());
        this.grupos.remove(g);
    }
    
    public Boolean canEntrarGrupo(ModelGrupo grupo) {
        return this.horarios.isLivre(grupo.getHorarios()) && grupo.getLevelMinimo() <= this.level;
    }
    
    public Boolean joinGrupo(ModelGrupo grupo) {
        if(this.canEntrarGrupo(grupo)) {
            System.out.println(this.toString() + " pode entrar no grupo.");
            this.horarios.ocupar(grupo.getHorarios());
            this.addGrupo(grupo);
            return true;
        } else {
            return false;
        }
    }
    
    public String toString() {
        String temp = "Jogador: " + this.nome_jogador + "\n";
        temp += "Personagem: " + this.nome_personagem + "\n";
        temp += "Raça: " + this.raca + "\n";
        temp += "Classe: " + this.classe + "\n";
        temp += "Profissão: " + this.profissao + "\n";
        temp += "Level: " + this.level + "\n";
        temp += "Level Profissão: " + this.profissao_level + "\n";
        temp += "Horarios: \n" + this.horarios + "\n";
        return temp;
    }
}
