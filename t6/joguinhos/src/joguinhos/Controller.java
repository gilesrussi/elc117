/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joguinhos;

import java.util.ArrayList;
import java.util.Random;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

enum Classes {
    Espadachim,
    Arqueiro,
    Gatuno,
    Acólito,
    SuperAprendiz,
    Arquimago
}

enum Racas {
    Zumbi,
    Gato,
    Leopardo,
    Dinossauro,
    ET,
    Pokemon
}

enum Profissoes {
    Mercador,
    Fazendeiro,
    Puzzlemaster,
    Zelador,
    Porteiro
}

enum Especialidades {
    Tanker,
    Healer,
    FDPs,
    MDPs,
    Stealth
}

/**
 *
 * @author G
 */
public class Controller {
    TelaPrincipal view;
    ModelPersonagem modelPersonagem;
    ModelGuilda modelGuilda;
    TablePersonagem tablePersonagem;
    TableGrupo tableGrupo;
    TablePersonagem tableGrupoHasPersonagem;
    ModelPersonagem uPersonagem;
    Random r;
    
    public Controller(TelaPrincipal view) {
        r = new Random();
        this.view = view;
        this.tablePersonagem = new TablePersonagem();
        this.tableGrupo = new TableGrupo();
        this.tableGrupoHasPersonagem = new TablePersonagem();
        this.modelGuilda = new ModelGuilda();
    }
    
    protected Classes stringToClasse(String c) {
        switch(c) {
            case "Acólito":
                return Classes.Acólito;
            case "Arqueiro":
                return Classes.Arqueiro;
            case "Arquimago":
                return Classes.Arquimago;
            case "Gatuno":
                return Classes.Gatuno;
            case "SuperAprendiz":
                return Classes.SuperAprendiz;
        }
        return Classes.Espadachim;
    }
    
    protected Racas stringToRaca(String r) {
        switch(r) {
            case "Dinossauro":
                return Racas.Dinossauro;
            case "ET":
                return Racas.ET;
            case "Gato":
                return Racas.Gato;
            case "Leopardo":
                return Racas.Leopardo;
            case "Pokemon":
                return Racas.Pokemon;
        }
        return Racas.Zumbi;
    }
    
    protected Profissoes stringToProfissao(String p) {
        switch(p) {
            case "Fazendeiro":
                return Profissoes.Fazendeiro;
            case "Mercador":
                return Profissoes.Mercador;
            case "Porteiro":
                return Profissoes.Porteiro;
            case "Puzzlemaster":
                return Profissoes.Puzzlemaster;
           
        }
        return Profissoes.Zelador;
    }
    
    protected Especialidades stringToEspecialidade(String e) {
        switch (e) {
            case "FDPs":
                return Especialidades.FDPs;
            case "Healer":
                return Especialidades.Healer;
            case "MDPs":
                return Especialidades.MDPs;
            case "Stealth":
                return Especialidades.Stealth;
        }
        return Especialidades.Tanker;
    }
    
    protected String classesToString(Classes c) {
        switch(c) {
            case Espadachim:
                return "Espadachim";
            case Acólito:
                return "Acólito";
            case Arqueiro:
                return "Arqueiro";
            case Arquimago:
                return "Arquimago";
            case Gatuno:
                return "Gatuno";
            case SuperAprendiz:
                return "SuperAprendiz";
        }
        return "NDA";
    }
    
    protected String racaToString(Racas r) {
        switch(r) {
            case Dinossauro:
                return "Dinossauro";
            case ET:
                return "ET";
            case Gato:
                return "Gato";
            case Leopardo:
                return "Leopardo";
            case Pokemon:
                return "Pokemon";
            case Zumbi:
                return "Zumbi";
        }
        return "NDA";
    }
    
    protected String profissaoToString(Profissoes p) {
        switch(p) {
            case Fazendeiro:
                return "Fazendeiro";
            case Mercador:
                return "Mercador";
            case Porteiro:
                return "Porteiro";
            case Puzzlemaster:
                return "PuzzleMaster";
            case Zelador:
                return "Zelador";
        }
        return "NDA";
    }
    
    protected String especialidadeToString(Especialidades e) {
        switch (e) {
            case FDPs:
                return "FDPs";
            case Healer:
                return "Healer";
            case MDPs:
                return "MDPs";
            case Stealth:
                return "Stealth";
            case Tanker:
                return "Tanker";
        }
        return "NDA";
    }
    
    private ModelHorario tableToHorario(ArrayList<JTable> h) {
        ModelHorario horario = new ModelHorario();
        for(int i = 0, size = h.size(); i < size; i++) {
            int[] temp = h.get(i).getSelectedRows();
            for(int j = 0; j < temp.length; j++) {
                horario.liberar(i, temp[j]);
            }
        }
        return horario;
    }
    
    public void salvarPersonagem() {
        ModelPersonagem personagem = new ModelPersonagem();
        this.salvarPersonagem(personagem);
        modelGuilda.addPersonagem(personagem);
        tablePersonagem.setTableData(modelGuilda.getPersonagens());
    }
    
    public void removerPersonagem(int row) {
        ModelPersonagem p = modelGuilda.getPersonagens().get(row);
        modelGuilda.removePersonagem(p);
        tablePersonagem.fireTableDataChanged();
    }
    
    public void salvarPersonagem(ModelPersonagem personagem) {
        if(personagem == null) {
            personagem = uPersonagem;
        }
        personagem.setNome_jogador(view.getNomeJogador().getText());
        personagem.setNome_personagem(view.getNomePersonagem().getText());
        personagem.setLevel(Integer.parseInt(view.getLevel().getText()) + 0);
        personagem.setRaca(this.racaToString( (Racas) view.getRaca().getSelectedItem()));
        personagem.setClasse(this.classesToString( (Classes) view.getClasse().getSelectedItem()));
        personagem.setProfissao(this.profissaoToString( (Profissoes) view.getProfissao().getSelectedItem()));
        personagem.setProfissao_level(Integer.parseInt(view.getProfissaoLevel().getText()) + 0);
        personagem.setEspecialidade(this.especialidadeToString( (Especialidades) view.getEspecialidade().getSelectedItem()));
        personagem.setHorarios(this.tableToHorario(view.getHorarios()));
        tablePersonagem.fireTableDataChanged();
        view.getBotaoAtualizarPersonagem().setEnabled(false);
        this.limparPersonagem();
    }
    
    public void gerarPersonagemAleatório() {
        RandomEnum<Classes> c = new RandomEnum<Classes>(Classes.class);
        RandomEnum<Racas> r = new RandomEnum<Racas>(Racas.class);
        RandomEnum<Profissoes> p = new RandomEnum<Profissoes>(Profissoes.class);
        RandomEnum<Especialidades> e = new RandomEnum<Especialidades>(Especialidades.class);
        ModelPersonagem personagem = new ModelPersonagem();
        personagem.setNome_jogador(this.gerarNomeAleatorio());
        personagem.setNome_personagem(this.gerarNomeAleatorio());
        personagem.setLevel(this.r.nextInt(100));
        personagem.setClasse(this.classesToString(c.random()));
        personagem.setEspecialidade(this.especialidadeToString(e.random()));
        personagem.setRaca(this.racaToString(r.random()));
        personagem.setProfissao(this.profissaoToString(p.random()));
        personagem.setProfissao_level(this.r.nextInt(100));
        ModelHorario horarioRandom = new ModelHorario();
        for(int i = 0; i < 7; i++) {
            for(int j = 0; j < 24; j++) { 
                if(this.r.nextInt(100) > 70) {
                    horarioRandom.liberar(i, j);
                }
            }
        }
        personagem.setHorarios(horarioRandom);
        this.modelGuilda.addPersonagem(personagem);
        this.tablePersonagem.setTableData(this.modelGuilda.getPersonagens());
        this.tablePersonagem.fireTableDataChanged();
    }
    
    private String gerarNomeAleatorio() {
        String[] cons = {"", "k", "s", "t", "n", "h", "m", "y", "r", "w"};
        String[] vowel = {"a", "i", "u", "e", "o", "ya", "yu", "yo"};
        String nome = "";
        for(int i = 0, size = r.nextInt(3) + 2; i < size; i++) {
            nome += cons[r.nextInt(cons.length)];
            nome += vowel[r.nextInt(vowel.length)];
        }
        return nome;
    }
    
    public void loadPersonagem(int row) {
        this.uPersonagem = modelGuilda.getPersonagens().get(row);
        view.getNomeJogador().setText(this.uPersonagem.getNome_jogador());
        view.getNomePersonagem().setText(this.uPersonagem.getNome_personagem());
        view.getLevel().setText(this.uPersonagem.getLevel().toString());
        view.getRaca().setSelectedItem(this.stringToRaca(this.uPersonagem.getRaca()));
        view.getClasse().setSelectedItem(this.stringToClasse(this.uPersonagem.getClasse()));
        view.getProfissao().setSelectedItem(this.stringToProfissao(this.uPersonagem.getProfissao()));
        view.getProfissaoLevel().setText(this.uPersonagem.getProfissao_level().toString());
        view.getEspecialidade().setSelectedItem(this.stringToEspecialidade(this.uPersonagem.getEspecialidade()));
        ArrayList<JTable> phorarios = view.getHorarios();
        Integer[][] horarios = this.uPersonagem.getHorarios().getHorarios();
        view.getBotaoAtualizarPersonagem().setEnabled(true);
        for(int i = 0; i < 7; i++) {
            ListSelectionModel model = phorarios.get(i).getSelectionModel();
            for(int j = 0; j < 24; j++) {
                if(horarios[i][j] != 0) {
                    model.addSelectionInterval(j, j);
                }
            }
        }
        view.getTabbedPane().setSelectedIndex(1);
        //System.out.println(modelGuilda.getPersonagens().get(row));
    }
    
    public void limparPersonagem() {
        view.getNomeJogador().setText("");
        view.getNomePersonagem().setText("");
        view.getLevel().setText("0");
        view.getRaca().setSelectedIndex(0);
        view.getClasse().setSelectedIndex(0);
        view.getProfissao().setSelectedIndex(0);
        view.getProfissaoLevel().setText("0");
        view.getEspecialidade().setSelectedIndex(0);
        for(JTable dia : view.getHorarios()) {
            dia.clearSelection();
        }
    }
    
    private static class RandomEnum<E extends Enum> {

        private static final Random RND = new Random();
        private final E[] values;

        public RandomEnum(Class<E> token) {
            values = token.getEnumConstants();
        }

        public E random() {
            return values[RND.nextInt(values.length)];
        }
    }
    
    public void removerGrupo(int row) {
        ModelGrupo g = modelGuilda.getGrupos().get(row);
        modelGuilda.removeGrupo(g);
        tableGrupo.fireTableDataChanged();
    }
    
    public void gerarGrupo() {
        ModelGrupo g = new ModelGrupo();
        g.setLevelMinimo(Integer.parseInt(view.getLevelMinimo().getText()));
        g.setMembrosMax(Integer.parseInt(view.getNumeroIntegrantes().getText()));
        g.setHorarios(this.tableToHorario(view.getHorariosGrupo()));
        if(g.addMembros(modelGuilda.getPersonagens())) {
            System.out.println("Grupo criado com sucesso!");
            modelGuilda.addGrupo(g);
            tableGrupo.setTableData(modelGuilda.getGrupos());
            tableGrupo.fireTableDataChanged();
            tableGrupoHasPersonagem.setTableData(g.getMembros());
            tableGrupoHasPersonagem.fireTableDataChanged();
        } else {
            System.out.println("FALHA MORTAL");
        }
    }
}
