/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package joguinhos;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author SAA
 */
public class TableGrupo extends AbstractTableModel {
    
    private String[] columnNames = {"Level Mínimo", "Número de Integrantes", "Número Máximo"};
    
    private ArrayList<ModelGrupo> grupos = null;
    
    public void setTableData(ArrayList<ModelGrupo> ps) {
        this.grupos = ps;
    }
    
    @Override
    public int getRowCount() {
        if(grupos != null) {
            return grupos.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ModelGrupo g = grupos.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return g.getLevelMinimo();
            case 1:
                return g.getMembrosNum();
            default:
                return g.getMembrosMax();
        }
    }
    
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
}
