/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package joguinhos;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author SAA
 */
public class TablePersonagem extends AbstractTableModel {
    
    private String[] columnNames = {"Nome Jogador", "Nome Personagem", "Level", "Classe", "Raça", "Profissão", "PLvl", "Espec."};
    
    private ArrayList<ModelPersonagem> personagens = null;
    
    public void setTableData(ArrayList<ModelPersonagem> ps) {
        this.personagens = ps;
    }
    
    @Override
    public int getRowCount() {
        if(personagens != null) {
            return personagens.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ModelPersonagem p = personagens.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return p.getNome_jogador();
            case 1:
                return p.getNome_personagem();
            case 2:
                return p.getLevel();
            case 3:
                return p.getClasse();
            case 4:
                return p.getRaca();
            case 5:
                return p.getProfissao();
            case 6:
                return p.getProfissao_level();
            case 7:
                return p.getEspecialidade();
            default:
                return "FODEL MANOLO!";
        }
    }
    
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
}
