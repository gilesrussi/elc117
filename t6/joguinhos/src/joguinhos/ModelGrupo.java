/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package joguinhos;

import java.util.ArrayList;

/**
 *
 * @author SAA
 */
public class ModelGrupo {
    
    private Integer levelMinimo;
    private Integer membrosMax;
    private Integer membrosNum;
    private ArrayList<ModelPersonagem> membros;
    private ModelHorario horarios;
    
    public ModelGrupo() {
        membros = new ArrayList<ModelPersonagem>();
        membrosNum = 0;
    }

    public Integer getLevelMinimo() {
        return levelMinimo;
    }

    public void setLevelMinimo(Integer levelMinimo) {
        this.levelMinimo = levelMinimo;
    }

    public Integer getMembrosMax() {
        return membrosMax;
    }

    public void setMembrosMax(Integer membrosMax) {
        this.membrosMax = membrosMax;
    }

    public Integer getMembrosNum() {
        return membrosNum;
    }

    public ArrayList<ModelPersonagem> getMembros() {
        return membros;
    }

    public void setMembros(ArrayList<ModelPersonagem> membros) {
        this.membros = membros;
    }
     
    public Boolean addMembros(ArrayList<ModelPersonagem> ap) {
        Integer membrosCanJoin = 0;
        for(ModelPersonagem p : ap) {
            if(p.canEntrarGrupo(this)) {
                membrosCanJoin++;
            }
        }
        if(membrosCanJoin >= membrosMax) {
            for(ModelPersonagem p : ap) {
                if(p.joinGrupo(this)) {
                    this.addMembro(p);
                    if(this.membrosNum == this.membrosMax) {
                        break;
                    }
                }
            }
            return true;
        }
        return false;
    }
    
    public void addMembro(ModelPersonagem p) {
        if(membrosNum < membrosMax) {
            membros.add(p);
            membrosNum++;
        }
    }
    
    public Boolean removeMembro(ModelPersonagem p) {
        return membros.remove(p);
    }
    
    public ModelHorario getHorarios() {
        return horarios;
    }
    
    public void setHorarios(ModelHorario h) {
        horarios = h;
    }
}
