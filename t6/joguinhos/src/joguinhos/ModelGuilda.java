/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package joguinhos;

import java.util.ArrayList;

/**
 *
 * @author SAA
 */
public class ModelGuilda {
    private ArrayList<ModelPersonagem> personagens;
    private ArrayList<ModelGrupo> grupos;
    
    public ModelGuilda() {
        personagens = new ArrayList<ModelPersonagem>();
        grupos = new ArrayList<ModelGrupo>();
    }
    
    public void addPersonagem(ModelPersonagem p) {
        personagens.add(p);
    }
    
    public Boolean removePersonagem(ModelPersonagem p) {
        return personagens.remove(p);
    }
    
    public ArrayList<ModelPersonagem> getPersonagens() {
        return this.personagens;
    }
    
    public void addGrupo(ModelGrupo g) {
        grupos.add(g);
    }
    
    public Boolean removeGrupo(ModelGrupo g) {
        for(ModelPersonagem p : g.getMembros()) {
            p.removeGrupo(g);
        }
        return grupos.remove(g);
    }
    
    public ArrayList<ModelGrupo> getGrupos() {
        return this.grupos;
    }
}
