package animator;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;

// Uma imagem na animacao.
class Image extends Figure{

    BufferedImage img;

    public Image(Point pos, BufferedImage img, movementFunction path) {
        this.pos = pos;
        this.img = img;
        this.dim = new Dimension(img.getHeight(), img.getWidth());
        this.mov = path;
        //this.setMovementFunction(new linearHorizontal(5));
    }


    
    public void draw(Graphics g) {
        g.drawImage(img, (int) pos.getX(), (int) pos.getY(), null);

    }

}
