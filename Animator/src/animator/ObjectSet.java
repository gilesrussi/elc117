package animator;

import static animator.AnimatorController.movementToFunction;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;



// Conjunto de objetos da animacao.
class ObjectSet {
    
    private List<Figure> figures;
    private Random rnd;
    
    public ObjectSet() {
        figures = new ArrayList<Figure>();
        rnd = new Random();
    }
    
        
    // Adiciona objetos da classe Image ao ObjectSet.
    // O codigo abaixo eh somente um teste e precisa ser substituido.
    void addImages(int n, Dimension dim, BufferedImage image, movementType path) {
        for (int i = 0; i < n; i++) {
            movementFunction mf = movementToFunction(path);
            Point p = new Point(rnd.nextInt(dim.width - image.getWidth()),rnd.nextInt(dim.height - image.getHeight()));
            figures.add(new Image(p, image, mf));
        }
    }
    
    // Adiciona objetos da classe Star ao ObjectSet.
    // O codigo abaixo eh somente um teste e precisa ser substituido.
    void addSpaceShip(int n, Dimension dim, movementType path) {
        for(int i = 0; i < n; i++) {
            movementFunction mf = movementToFunction(path);
            Dimension d = new Dimension(rnd.nextInt(100)+50, rnd.nextInt(100)+50);
            Point p = new Point(rnd.nextInt(dim.width - d.width), rnd.nextInt(dim.height - d.height));
            Figure ss = new SpaceShip(p, d);
            ss.setMovementFunction(mf);
            
            figures.add(ss);
        }
    }
    
    // Adiciona objetos da classe Star ao ObjectSet.
    // O codigo abaixo eh somente um teste e precisa ser substituido.
    void addRectangles(int n, Dimension dim, movementType path) {
        for(int i = 0; i < n; i++) {
            movementFunction mf = movementToFunction(path);
            Dimension d = new Dimension(rnd.nextInt(100)+50, rnd.nextInt(100)+50);
            Point p = new Point(rnd.nextInt(dim.width - d.width), rnd.nextInt(dim.height - d.height));
            Figure rect = new Rectangle(p, d);
            rect.setMovementFunction(mf);
            
            figures.add(rect);
        }
    }
    
    // Adiciona objetos da classe Star ao ObjectSet.
    // O codigo abaixo eh somente um teste e precisa ser substituido.
    void addCircles(int n, Dimension dim, movementType path) {
        for(int i = 0; i < n; i++) {
            movementFunction mf = movementToFunction(path);
            Dimension d = new Dimension(rnd.nextInt(100)+50, rnd.nextInt(100)+50);
            Point p = new Point(rnd.nextInt(dim.width - d.width), rnd.nextInt(dim.height - d.height));
            Figure circle = new Circle(p, d);
            circle.setMovementFunction(mf);
            
            figures.add(circle);
        }
    }
    
    void addPacMan(int n, Dimension dim, movementType path) {
        for(int i = 0; i < n; i++) {
            movementFunction mf = movementToFunction(path);
            Dimension d = new Dimension(rnd.nextInt(100)+50, rnd.nextInt(100)+50);
            Point p = new Point(rnd.nextInt(dim.width - d.width), rnd.nextInt(dim.height - d.height));
            Figure pacman = new PacMan(p, d);
            pacman.setMovementFunction(mf);
            
            figures.add(pacman);
        }
    }
    
    // Desenha cada um dos objetos da animacao.
    // O codigo abaixo eh somente um teste e precisa ser substituido.
    void drawAll(Graphics g) {
        for(Figure i : figures) {
            i.draw(g);
        }
    }

    // Move cada um dos objetos da animacao.
    // O codigo abaixo eh somente um teste e precisa ser substituido.
    void moveAll(Dimension dim) {
        for(Figure i : figures) {
            i.move(dim);
        }
    }
}
