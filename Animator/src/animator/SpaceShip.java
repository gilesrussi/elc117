/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author G
 */
public class SpaceShip extends Figure {
    private Dimension dim;
    private ArrayList<Rectangle> pieces; 
    
    
    SpaceShip(Point pos, Dimension dim) {
        pieces = new ArrayList<Rectangle>();
        Point posLeftWing = new Point(pos.x, pos.y + (dim.height / 3));
        Dimension dimLeftWing = new Dimension(dim.width / 6, dim.height / 2);
        pieces.add(new Rectangle(posLeftWing, dimLeftWing));
        Point posBody = new Point(pos.x, pos.y + (5 * dim.height / 6));
        Dimension dimBody = new Dimension(dim.width, dim.height / 6);
        pieces.add(new Rectangle(posBody, dimBody));
        Point posCenterWing = new Point(pos.x + (dim.width / 3), pos.y);
        Dimension dimCenterWing = new Dimension(dim.width / 3, 5 * dim.height / 6);
        pieces.add(new Rectangle(posCenterWing, dimCenterWing));
        Point posRightWing = new Point(pos.x + 5 * dim.width / 6, pos.y + (dim.height / 3));
        Dimension dimRightWing = new Dimension(dim.width / 6, dim.height / 2);
        pieces.add(new Rectangle(posRightWing, dimRightWing));
        
        this.pos = pos;
        this.dim = dim;
    }
    
    @Override
    void draw(Graphics g) {
        for(int i = 0; i < pieces.size(); i++) {
            pieces.get(i).draw(g);
        }
    }
    
    void move(Dimension dim) {
        for(int i = 0; i < pieces.size(); i++) {
            mov.makeItMove(pieces.get(i), dim);
        }
        mov.increaseCounter();
    }

    
}
