/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animator;

import java.awt.Dimension;
import java.awt.Point;

/**
 *
 * @author G
 */
abstract public class movementFunction {
    
    protected Integer cont = 1;
    
    abstract public void makeItMove(Figure f, Dimension containerDim);
    
    public void keepItIn(Figure f, Dimension containerDim) {
        Point p = f.getPos();
        Dimension d = f.getDim();
        if(p.x > containerDim.width) {
            p.x = 0;
        } else if (p.x < 0) {
            p.x = containerDim.width;
        }
        if(p.y > containerDim.height) {
            p.y = 0;
        } else if(p.y < 0) {
            p.y = containerDim.height;
        }
        
        //f.setPos(p);
    }
    
    public void increaseCounter() {
        cont = (cont + 1) % 10;
    }
}
