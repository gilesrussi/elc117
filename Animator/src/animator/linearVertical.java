/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animator;

import java.awt.Dimension;
import java.awt.Point;
import java.util.Random;

/**
 *
 * @author G
 */
public class linearVertical extends movementFunction {
    
    private Integer speed;
    
    public linearVertical() {
        speed = new Random().nextInt(10)+1;
    }

    public linearVertical(Integer n) {
        speed = n;
    }
    
    @Override
    public void makeItMove(Figure f, Dimension containerDim) {
        Point p = f.getPos();
        p.y += this.speed;
        keepItIn(f, containerDim);
    }
    
}
