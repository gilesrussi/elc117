/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animator;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

/**
 *
 * @author G
 */
abstract public class Figure {
    protected Point pos;
    protected Dimension dim;
    protected movementFunction mov;
    
    public Point getPos() {
        return pos;
    }
    
    public Dimension getDim() {
        return dim;
    }
    
    public void setMovementFunction(movementFunction m) {
        mov = m;
    }
    
    public movementFunction getMovementFuncition() {
        return mov;
    }
    
    public void setDim(Dimension d) {
        dim = d;
    }
    
    public void setPos(Point p) {
        this.pos = p;
    }
    
    abstract void draw(Graphics g);
    
    void move(Dimension dim) {
        mov.makeItMove(this, dim);
        mov.increaseCounter();
    }
}
