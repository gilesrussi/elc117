/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animator;

import java.awt.Dimension;
import java.awt.Point;
import java.util.Random;

/**
 *
 * @author G
 */
public class zigZag extends movementFunction {
    
    private Integer xspeed;
    private Integer yspeed;
    
    
    public zigZag() {
        xspeed = new Random().nextInt(10)+1;
        yspeed = new Random().nextInt(30)+10;
        cont = 1;
    }

    
    @Override
    public void makeItMove(Figure f, Dimension containerDim) {
        Point p = f.getPos();
        p.x += this.xspeed;
        if(cont%10 == 0) {
            yspeed = - yspeed;
            cont++;
        }
        p.y += this.yspeed;
        
        
        keepItIn(f, containerDim);
    }
    
    
}
