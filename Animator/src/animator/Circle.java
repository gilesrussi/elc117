/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

/**
 *
 * @author G
 */
public class Circle extends Figure {
    private Dimension dim;
    
    
    Circle(Point pos, Dimension dim) {
        this.pos = pos;
        this.dim = dim;
    }
    
    @Override
    void draw(Graphics g) {
        g.setColor(Color.red);
        g.fillOval(pos.x, pos.y, dim.width, dim.height);
    }

    
}
