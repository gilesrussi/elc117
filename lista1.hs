-- EXECUCAO

-- Soma os quadrados de 2 numeros
doubleUs :: Int -> Int -> Int   
doubleUs x y = x^2 + y^2
  
-- Verifica se um numero eh par (mod retorna resto da divisao inteira)
isEven :: Int -> Bool
isEven n = if mod n 2 == 0 then True else False
  
-- Gera um numero a partir de um caracter
encodeMe :: Char -> Int
encodeMe c 
  | c == 'S'  = 0
  | c == 'N'  = 1
  | otherwise = 2
  
-- Calcula o quadrado do primeiro elemento da lista
doubleFirst :: [Int] -> Int
doubleFirst lis = (head lis)^2


-- FIM DA EXECUCAO

-- PROGRAMACAO

-- 1. Escreva uma fun��o hasEqHeads :: [Int] -> [Int] -> Bool que verifica se as 2 listas possuem o mesmo primeiro elemento. Exemplo de uso:

hasEqHeads :: [Int] -> [Int] -> Bool
hasEqHeads (x:_) (y:_) = x == y

-- 2. Observe a fun��o abaixo, que eleva ao cubo cada elemento da lista, produzindo outra lista.

pot3 :: [Int] -> [Int]
pot3 [] = []
pot3 ns = (head ns)^3 : pot3 (tail ns)

-- 3. Escreva uma fun��o recursiva add10, que adicione a constante 10 a cada elemento de uma lista, produzindo outra lista.

add10 :: [Int] -> [Int]
add10 [] = []
add10 (x:xs) = (x+10) : add10 (xs)

-- 4. Escreva uma fun��o recursiva addComma, que adicione uma v�rgula no final de cada string contida numa lista. Dica: (1) Strings s�o listas de caracteres. (2) Para concatenar listas, use o operador ++.

addComma :: [String] -> [String]
addComma [] = []
addComma (x:xs) = (x ++ ",") : addComma xs

-- 5. Refa�a os 2 exerc�cios anteriores usando a fun��o de alta ordem 'map'.

add10Map :: [Int] -> [Int]
add10Map x = map (+10) x

addCommaMap :: [String] -> [String]
addCommaMap x = map (++",") x

-- 6. Crie uma fun��o htmlListItems :: [String] -> [String], que receba uma lista de strings e retorne outra lista contendo as strings formatadas como itens de lista em HTML. Dica: use map e defina uma fun��o auxiliar a ser aplicada a cada elemento. Exemplo de uso da fun��o:

htmlListItems :: [String] -> [String]
htmlListItems x = map (\s -> "<LI>" ++ s ++ "</LI>") x

-- 7. Crie uma fun��o recursiva charFound :: Char -> String -> Bool, que verifique se o caracter (primeiro argumento) est� contido na string (segundo argumento). Exemplos de uso da fun��o:

charFound :: Char -> String -> Bool
charFound _ [] = False
charFound c (x:xs) = c == x || charFound c xs

-- 8. Reescreva a fun��o anterior sem recurs�o, usando outras fun��es pr�-definidas j� vistas em aula.

charFoundFilter :: Char -> String -> Bool
charFoundFilter c s = (length (filter (== c) s)) > 0

-- 9. Use a fun��o de alta ordem 'zipWith' para produzir uma fun��o que obtenha as diferen�as, par a par, dos elementos de duas listas. Por exemplo: para listas de entrada [1,2,3,4] e [2,2,1,1], o resultado ser� [-1,0,2,3].

diffNumList :: [Int] -> [Int] -> [Int]
diffNumList x y = zipWith (-) x y

-- FIM DA PROGRAMA��O



-- FUNCOES DE ALTA ORDEM

-- 1. Dada uma lista de n�meros, calcular 2*n+1 para cada n�mero n contido na lista.
doublePlusOne :: [Int] -> [Int]
doublePlusOne x = map (\n -> 2 * n + 1) x

-- 2. Dadas duas listas X e Y de n�meros inteiros, calcular 4*x+2*y+1 para cada par de n�meros x e y pertencentes �s listas. Exemplo:
funFunction :: [Int] -> [Int] -> [Int]
funFunction x y = zipWith (\x y -> 4 * x + 2 * y + 1) x y

-- 3. Dada uma lista de strings, produzir outra lista com strings de 10 caracteres, usando o seguinte esquema: strings de entrada com mais de 10 caracteres s�o truncadas, strings com at� 10 caracteres s�o completadas com '.' at� ficarem com 10 caracteres. Exemplo:

toTen :: String -> String
toTen s 
   | length s > 10    = take 10 s
   | length s == 10   = s
   | length s < 10    = s ++ (replicate (10 - length s) '.')

toTenList :: [String] -> [String]
toTenList s = map (toTen) s

-- 4. Dada uma lista de idades, selecionar as que s�o maiores que 20 e, para cada uma, calcular o ano de nascimento correspondente (aproximado, considerando o ano atual).

birthYear :: [Int] -> [Int]
birthYear x = map (2015-) (filter (>20) x)