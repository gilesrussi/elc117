geraPotencias :: Int -> [Int]
geraPotencias x =  [2^y | y <- [x, x-1 .. 0]]

addSuffix :: String -> [String] -> [String]
addSuffix suf lis = [x ++ suf | x <- lis] 

anosDeNascimento :: [Int] -> [Int]
anosDeNascimento anos = [2015-x | x <-anos, x > 20]