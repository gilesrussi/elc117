import Data.Char

-- 1. Escreva uma fun��o recursiva firstName :: String -> String que, dado o nome completo de uma pessoa, obtenha seu primeiro nome. Suponha que cada parte do nome seja separada por um espa�o e que n�o existam espa�os no in�cio ou fim do nome.

firstName :: String -> String
firstName (' ':_) = ""
firstName (s:sx) = s : firstName sx

-- 2. Escreva uma fun��o firstName' :: String -> String com o mesmo resultado do exerc�cio anterior, mas sem usar recurs�o. Dica: estude fun��es pr�-definidas em Haskell (List operations -> Sublists) em http://hackage.haskell.org/package/base-4.8.0.0/docs/Prelude.html.
firstName' :: String -> String
firstName' s = takeWhile (/= ' ') s


-- 3. Escreva uma fun��o lastName :: String -> String que, dado o nome completo de uma pessoa, obtenha seu �ltimo sobrenome. Suponha que cada parte do nome seja separada por um espa�o e que n�o existam espa�os no in�cio ou fim do nome.
lastName :: String -> String
lastName s = reverse (firstName (reverse s))

-- 4. Escreva uma fun��o n�o-recursiva userName :: String -> String que, dado o nome completo de uma pessoa, crie um nome de usu�rio (login) da pessoa, formado por: primeira letra do nome seguida do sobrenome, tudo em min�sculas. Dica: estude as fun��es pr�-definidas no m�dulo Data.Char, para manipula��o de mai�sculas e min�sculas.
userName :: String -> String
userName (s:sx) = map (toLower) ( s : reverse ( takeWhile (/= ' ')(reverse sx) ) )

-- 5. Escreva uma fun��o n�o-recursiva encodeName :: String -> String que substitua vogais em uma string, conforme o esquema a seguir: a = 4, e = 3, i = 1, o = 0, u = 00.
encodeName :: String -> String
encodeName s = concatMap (\y -> if (toLower y) == 'a' then "4" else if (toLower y) == 'e' then "3" else if (toLower y) == 'i' then "1" else if (toLower y) == 'o' then "0" else if (toLower y) == 'u' then "00" else [y]) s

-- 6. Escreva uma fun��o isElem :: Int -> [Int] -> Bool que verifique se um dado elemento pertence a uma lista, conforme os exemplos abaixo:
isElem :: Int -> [Int] -> Bool
isElem _ [] = False
isElem e (x:xs) = x == e || isElem e xs

-- 7. Escreva uma fun��o recursiva que retorne o n�mero de vogais em uma string.
vowels :: [Char]
vowels = ['a', 'e', 'i', 'o', 'u']

isIn :: Char -> [Char] -> Bool
isIn _ [] = False
isIn c (x:xs) = x == c || isIn c xs

countVowel :: [Char] -> Integer
countVowel [] = 0;
countVowel (x:xs)
  | isIn (toLower x) vowels = 1 + countVowel xs
  | otherwise     = 0 + countVowel xs

-- 8. Escreva uma fun��o n�o-recursiva que retorne o n�mero de consoantes em uma string.
consonants :: [Char]
consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z']

countConsonant :: [Char] -> Int
countConsonant x = length (filter (==True)(map (\y -> isIn (toLower y) consonants) x))

-- 9. Escreva uma fun��o n�o-recursiva isInt :: String -> Bool que verifique se uma dada string s� cont�m d�gitos (0 a 9).
isInt :: String -> Bool
isInt x = foldl (&&) True (map (\s -> not (any (==s) (vowels ++ consonants))) x)

-- 10. Escreva uma fun��o n�o-recursiva strToInt :: String -> Int que converta uma string em um n�mero inteiro, fazendo opera��es aritm�ticas com seus d�gitos (p.ex.: "356" = 3*100 + 5*10 + 6*1 = 356). Considere que a string seja um n�mero v�lido, isto �, s� contenha d�gitos de 0 a 9. Dica: se n�o souber por onde come�ar, estude o exemplo de valida��o de CPF visto em aula. 
strToInt :: String -> Int
strToInt s = sum $ zipWith (*) (map (digitToInt) s) (map (10^) (reverse (take (length s) [0, 1..])))