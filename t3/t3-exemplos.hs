import Data.Char

-- As fun��es de alta ordem any e all s�o pr�-definidas na biblioteca Prelude do Haskell (veja se��o Special Folds). Estude e teste essas fun��es e apresente 2 exemplos de uso de cada uma delas. 
possuiC :: String -> Bool
possuiC s = any (=='c') s

maiorNoveMil :: [Integer] -> Bool
maiorNoveMil x = any (>9000) x

soC :: String -> Bool
soC s = all (=='c') s

allCaps :: String -> Bool
allCaps s = all (isUpper) s

-- Em Haskell, o s�mbolo '$' pode ser usado para escrever c�digos ainda mais curtos. Descubra seu significado e apresente 2 exemplos de uso. 
isInt :: String -> Bool
isInt x = foldl (&&) True $ map (\s -> not (any (==s) (['a' .. 'z']))) x

maiorNoveMilCash :: [Float] -> [Float]
maiorNoveMilCash x = map (/2) (filter (>18000) $ map (*2) x)

-- Haskell permite composi��o de fun��es, exatamente como em matem�tica. Descubra como fazer isso e apresente 2 exemplos de aplica��o de fun��es compostas. 
doubleAbsolute :: [Integer] -> [Integer]
doubleAbsolute x = map ((*2) . abs) x

doubleALot :: [Integer] -> [Integer]
doubleALot x = map ((*2) . (*2) . (*2)) x