import Data.Char

userName :: String -> String
userName (s:sx) = map (toLower) ( s : reverse ( takeWhile (/= ' ')(reverse sx) ) )

main :: IO ()
main = do
    strcontent <- readFile "nomes.csv"
    let strlist = lines strcontent
        strnew = map (\y -> y ++ "," ++ userName y) strlist
    writeFile "logins.csv" (unlines strnew)
