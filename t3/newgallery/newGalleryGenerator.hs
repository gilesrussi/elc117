import Data.List

--
-- Este programa ilustra:
-- 1) Uso de registros (record syntax) em Haskell
-- 2) Leitura de arquivo CSV
--

--
-- Declara novo tipo de dado 'GalleryItem' usando record syntax
-- Cada GalleryItem é um registro formado por 3 campos / atributos
-- Com esta sintaxe, Haskell automaticamente cria funções de acesso aos campos
-- Por exemplo, dado um GalleryItem x, 'title x' vai retornar o atributo 'title' de x,
-- 'description x' vai retornar o atributo 'description' de x, e assim por diante
-- Mais sobre isso em: http://learnyouahaskell.com/making-our-own-types-and-typeclasses
--
data GalleryItem =
   GalleryItem {title :: String,
                description :: String,
                author :: String,
                repository :: String,
                semester :: String,
                image :: String,
                alt :: String}
               
-- Converte uma lista de String em um GalleryItem
toGalleryItem :: [String] -> GalleryItem
toGalleryItem [s1, s2, s3, s4, s5, s6, s7] = GalleryItem {title = s1, description = s2, author = s3, repository = s4, semester = s5, image = s6, alt = s7}

replaceTag :: String -> String -> String -> String
replaceTag tag content htmltemplate = replace ("<!--@" ++ tag ++ "-->") content htmltemplate

buildItems :: String -> [GalleryItem] -> String
buildItems imageTemplate gi = concat $ map (\e -> replaceTag "alt" (alt e) $ replaceTag "image" (image e) $ replaceTag "semester" (semester e) $ replaceTag "repository" (repository e) $ replaceTag "author" (author e) $ replaceTag "description" (description e) $ replaceTag "title" (title e) imageTemplate) gi



-- Substitui old por new em list
replace :: Eq a => [a] -> [a] -> [a] -> [a]
replace old new list = intercalate new . splitOn old $ list

splitOn :: Eq a => [a] -> [a] -> [[a]]
splitOn []    _  = error "splitOn: empty delimiter"
splitOn delim xs = loop xs
    where loop [] = [[]]
          loop xs | delim `isPrefixOf` xs = [] : splitOn delim (drop len xs)
          loop (x:xs) = let (y:ys) = splitOn delim xs
                         in (x:y) : ys
          len = length delim

-- Funcao principal que faz leitura de arquivo e mostra atributos de um item da galeria
main :: IO ()
main = do
    strcontent <- readFile infile                               -- le conteudo do arquivo em string
    htmltemplate <- readFile template
    imagetemplate <- readFile itemplate
    let strlist = map (splitOnChar ';') (lines strcontent)      -- extrai linhas e quebra cada uma delas
        itemlist = map (toGalleryItem) strlist                  -- transforma cada linha num GalleryItem
        galleryContent = buildItems imagetemplate itemlist
        fullhtml = replaceTag "content" galleryContent htmltemplate
    putStrLn ("jah eh")
    writeFile outfile fullhtml
    where 
    infile = "data.csv"
    outfile = "gallery.html"
    template = "template.html"
    itemplate = "image_template.html"


-- Funcao que decompoe string usando um caracter delimitador
splitOnChar :: Char -> String -> [String]
splitOnChar x y = auxSplitOnChar x y [[]]

auxSplitOnChar :: Char -> String -> [String] -> [String]
auxSplitOnChar x [] z = reverse (map reverse z)
auxSplitOnChar x (y:ys) (z:zs) = 
    if y == x then 
        auxSplitOnChar x ys ([]:(z:zs)) 
    else 
        auxSplitOnChar x ys ((y:z):zs)
            
