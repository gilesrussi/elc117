import Text.Printf
import Data.List

--
-- Este programa l� um arquivo CSV, com 2 dados por linha. Cada linha � transformada numa tupla. 
-- A lista de tuplas � passada para uma fun��o que vai gerar uma longa string representando um
-- documento HTML.
--

main :: IO ()
main = do
    strcontent <- readFile infile
    template <- readFile inTemplate
    let listofstrlist = map (splitOnChar ',') (lines strcontent)
        strtuplelist = map (\lis -> (head lis, last lis)) listofstrlist
    writeFile outfile (mkHtmlURLItemsDoc "Usuarios Cadastrados no NCC" strtuplelist template)
    where 
    infile = "logins.csv"
    outfile = "output.html"
    inTemplate = "template.html"

-- Pega a template e coloca titulo e conteudo :3
fillHtml :: String -> String -> String -> String
fillHtml htmltemplate content title = replace "<!--@title-->" title $ replace "<!--@content-->" content htmltemplate
    
-- Troca uma string por outra string
replace :: Eq a => [a] -> [a] -> [a] -> [a]
replace old new list = intercalate new . splitOn old $ list


splitOn :: Eq a => [a] -> [a] -> [[a]]
splitOn []    _  = error "splitOn: empty delimiter"
splitOn delim xs = loop xs
    where loop [] = [[]]
          loop xs | delim `isPrefixOf` xs = [] : splitOn delim (drop len xs)
          loop (x:xs) = let (y:ys) = splitOn delim xs
                         in (x:y) : ys
          len = length delim

-- Esta fun��o deve ser alterada para chamar outras fun��es que v�o
-- construir o documento HTML
mkHtmlURLItemsDoc :: String -> [(String,String)] -> String -> String
mkHtmlURLItemsDoc title lis template = fillHtml template (geraLi lis) "Usuarios Cadastrados no NCC\n"
--unlines (map fst lis)

-- Pega lista de tupla e devolve com tags bacanas :D
geraLi :: [(String,String)] -> String
geraLi lis = intercalate "\n" $ map (\(nome, login) -> "<li><a href='http://www.inf.ufsm.br/~" ++ login ++ "'>" ++ nome ++ "</a></li>") lis

-- Decompoe string usando um caracter delimitador
splitOnChar :: Char -> String -> [String]
splitOnChar x y = auxSplitOnChar x y [[]]

-- Auxilia fun��o acima.
auxSplitOnChar :: Char -> String -> [String] -> [String]
auxSplitOnChar x [] z = reverse (map reverse z)
auxSplitOnChar x (y:ys) (z:zs) = 
    if y == x then 
        auxSplitOnChar x ys ([]:(z:zs)) 
    else 
        auxSplitOnChar x ys ((y:z):zs)
